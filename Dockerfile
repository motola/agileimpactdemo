FROM node:9.11.1
WORKDIR /app
ADD . /app
RUN npm install && \
    npm run build
EXPOSE 3000
CMD npm start
